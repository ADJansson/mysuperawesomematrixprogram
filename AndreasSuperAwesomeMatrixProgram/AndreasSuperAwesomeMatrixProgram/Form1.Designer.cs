﻿namespace AndreasSuperAwesomeMatrixProgram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCalc = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.numA11 = new System.Windows.Forms.NumericUpDown();
            this.numA12 = new System.Windows.Forms.NumericUpDown();
            this.numA13 = new System.Windows.Forms.NumericUpDown();
            this.numA21 = new System.Windows.Forms.NumericUpDown();
            this.numA22 = new System.Windows.Forms.NumericUpDown();
            this.numA23 = new System.Windows.Forms.NumericUpDown();
            this.numA31 = new System.Windows.Forms.NumericUpDown();
            this.numA32 = new System.Windows.Forms.NumericUpDown();
            this.numA33 = new System.Windows.Forms.NumericUpDown();
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numA11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA33)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtOutput, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.62323F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.37677F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(422, 353);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnCalc, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.73077F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.26923F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(416, 208);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnCalc
            // 
            this.btnCalc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCalc.Location = new System.Drawing.Point(3, 172);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(410, 33);
            this.btnCalc.TabIndex = 0;
            this.btnCalc.Text = "&Calculate!";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel3.Controls.Add(this.numA11, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.numA12, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.numA13, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.numA21, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.numA22, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.numA23, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.numA31, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.numA32, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.numA33, 2, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(410, 163);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // numA11
            // 
            this.numA11.Location = new System.Drawing.Point(3, 3);
            this.numA11.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA11.Name = "numA11";
            this.numA11.Size = new System.Drawing.Size(120, 20);
            this.numA11.TabIndex = 0;
            // 
            // numA12
            // 
            this.numA12.Location = new System.Drawing.Point(139, 3);
            this.numA12.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA12.Name = "numA12";
            this.numA12.Size = new System.Drawing.Size(120, 20);
            this.numA12.TabIndex = 1;
            // 
            // numA13
            // 
            this.numA13.Location = new System.Drawing.Point(275, 3);
            this.numA13.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA13.Name = "numA13";
            this.numA13.Size = new System.Drawing.Size(120, 20);
            this.numA13.TabIndex = 2;
            // 
            // numA21
            // 
            this.numA21.Location = new System.Drawing.Point(3, 57);
            this.numA21.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA21.Name = "numA21";
            this.numA21.Size = new System.Drawing.Size(120, 20);
            this.numA21.TabIndex = 3;
            // 
            // numA22
            // 
            this.numA22.Location = new System.Drawing.Point(139, 57);
            this.numA22.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA22.Name = "numA22";
            this.numA22.Size = new System.Drawing.Size(120, 20);
            this.numA22.TabIndex = 4;
            // 
            // numA23
            // 
            this.numA23.Location = new System.Drawing.Point(275, 57);
            this.numA23.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA23.Name = "numA23";
            this.numA23.Size = new System.Drawing.Size(120, 20);
            this.numA23.TabIndex = 5;
            // 
            // numA31
            // 
            this.numA31.Location = new System.Drawing.Point(3, 111);
            this.numA31.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA31.Name = "numA31";
            this.numA31.Size = new System.Drawing.Size(120, 20);
            this.numA31.TabIndex = 6;
            // 
            // numA32
            // 
            this.numA32.Location = new System.Drawing.Point(139, 111);
            this.numA32.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA32.Name = "numA32";
            this.numA32.Size = new System.Drawing.Size(120, 20);
            this.numA32.TabIndex = 7;
            // 
            // numA33
            // 
            this.numA33.Location = new System.Drawing.Point(275, 111);
            this.numA33.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numA33.Name = "numA33";
            this.numA33.Size = new System.Drawing.Size(120, 20);
            this.numA33.TabIndex = 8;
            // 
            // txtOutput
            // 
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(3, 217);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(416, 133);
            this.txtOutput.TabIndex = 1;
            this.txtOutput.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 353);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Andreas\' SuperAwesome Matrix Program";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numA11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numA33)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.NumericUpDown numA11;
        private System.Windows.Forms.NumericUpDown numA12;
        private System.Windows.Forms.NumericUpDown numA13;
        private System.Windows.Forms.NumericUpDown numA21;
        private System.Windows.Forms.NumericUpDown numA22;
        private System.Windows.Forms.NumericUpDown numA23;
        private System.Windows.Forms.NumericUpDown numA31;
        private System.Windows.Forms.NumericUpDown numA32;
        private System.Windows.Forms.NumericUpDown numA33;
        private System.Windows.Forms.RichTextBox txtOutput;
    }
}

