﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AndreasSuperAwesomeMatrixProgram
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            // Do some heftig kalkulasjons sjø

            int size = 3; // for now

            float[] vector = new float[] { 1f, 1f, 1f };
            float[] scaledVector = new float[] { 1f, 1f, 1f };

            int[,]  matrix = new int[size, size];

            matrix[0, 0] = (int) numA11.Value;
            matrix[0, 1] = (int) numA12.Value;
            matrix[0, 2] = (int) numA13.Value;
            matrix[1, 0] = (int) numA21.Value;
            matrix[1, 1] = (int) numA22.Value;
            matrix[1, 2] = (int) numA23.Value;
            matrix[2, 0] = (int) numA31.Value;
            matrix[2, 1] = (int) numA32.Value;
            matrix[2, 2] = (int) numA33.Value;

            for (int i = 0; i <= 20; i++)
            {
                float v1 = matrix[0, 0] * scaledVector[0]
                + matrix[0, 1] * scaledVector[1]
                + matrix[0, 2] * scaledVector[2];

                float v2 = matrix[1, 0] * scaledVector[0]
                + matrix[1, 1] * scaledVector[1]
                + matrix[1, 2] * scaledVector[2];

                float v3 = matrix[2, 0] * scaledVector[0]
                + matrix[2, 1] * scaledVector[1]
                + matrix[2, 2] * scaledVector[2];

                float dominant = vector.Max();

                float quotient = (v1 * scaledVector[0] + v2 * scaledVector[1] + v3 * scaledVector[2])
                    / (scaledVector[0] * scaledVector[0] + scaledVector[1] * scaledVector[1] + scaledVector[2] * scaledVector[2]);

                double error = Math.Sqrt((v1 * v1 + v2 * v2 + v3 * v3) 
                    / (scaledVector[0] * scaledVector[0] + scaledVector[1] * scaledVector[1] + scaledVector[2] * scaledVector[2]) 
                    - quotient * quotient);

                scaledVector[0] = v1 / dominant;
                scaledVector[1] = v2 / dominant;
                scaledVector[2] = v3 / dominant;               

                vector[0] = v1;
                vector[1] = v2;
                vector[2] = v3;

                string output = "Step " + (i + 1) + ": q(" + i + ") = " + quotient 
                    + ", |e| = " + error + "\n";
                txtOutput.AppendText(output);

            }


        }
    }
}
